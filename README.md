# Anagrammer #

Anagrammer is a JavaFX app that generates multiword anagrams for you.  It runs on Windows, Mac OS X, and Linux.

![](src/test/resources/screenshot.png)

## Downloads ##

There are multiple versions available for download. 

#### [MSI Installer for Windows](https://bitbucket.org/_ijk/anagrammer/downloads)
An MSI installer for Windows, which has the Java runtime bundled inside. You do not need to have Java installed to use this version, but it's a larger download than the runnable JAR.  
  
#### [EXE (Innosetup) Installer for Windows](https://bitbucket.org/_ijk/anagrammer/downloads)
A native exe (Innosetup) installer for Windows, which has the Java runtime bundled inside. You do not need to have Java installed to use this version, but it's a larger download than the runnable JAR.  
  
#### [Cross-platform Runnable JAR](https://bitbucket.org/_ijk/anagrammer/downloads)
A cross-platform runnable JAR, which assumes you already have Java 8 installed.  To run the lightweight JAR file, you'll need a [Java Runtime Environment](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (JRE or JDK version 8 or greater) to run it.

## Building the Runnable JAR ##

To build the runnable JAR yourself, you'll need [Maven 3](http://maven.apache.org/).

### Build using Maven

Run the following from a command line:

`mvn clean install`

...and then run the `Anagrammer-X.Y.jar` file in the `/target` folder.

## Eclipse Project

An Eclipse project is also included. Run `mvn eclipse:eclipse` from a command line to update the project's classpath settings to work with your local repo.