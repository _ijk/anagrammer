package com.what.anagram.ui;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Compares two words first by length, then alphabetically.
 */
public final class WordLengthComparator implements Comparator<String>, Serializable {
	private static final long serialVersionUID = -8695932011558018548L;

	@Override
	public int compare(String a, String b) {
		int lengthCompare = Integer.compare(b.length(), a.length());
		if (lengthCompare != 0) {
			return lengthCompare;
		}

		return a.compareTo(b);
	}
}