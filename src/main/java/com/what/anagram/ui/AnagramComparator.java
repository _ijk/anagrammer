package com.what.anagram.ui;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Compares two anagrams first by number of words, and then alphabetically.
 */
public final class AnagramComparator implements Comparator<String>, Serializable {
	private static final long serialVersionUID = 4008802924901773338L;

	@Override
	public int compare(String a, String b) {
		// Count spaces in each string.
		int numA = getNumberOfWords(a);
		int numB = getNumberOfWords(b);

		if (numA == numB) {
			return a.compareTo(b);
		} else {
			return Integer.compare(numA, numB);
		}
	}

	private static int getNumberOfWords(String value) {
		value = value.trim();
		if (value.isEmpty()) {
			return 0;
		}
		return value.split("\\W+").length;
	}
}