package com.what.anagram.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Joiner;
import com.google.common.base.Strings;

public class Anagrammer {

	private final TrieNode root = new TrieNode(); // dummy root

	private final Map<Character, Integer> histogram;
	private final Set<String> dictionary;

	private final Set<String> includedWords = new HashSet<String>();
	private final Set<String> excludedWords = new HashSet<String>();

	private int maxAnagrams = Defaults.MAX_ANAGRAMS;
	private int maxWordsPerAnagram = Defaults.MAX_WORDS_PER_ANAGRAM;
	private int minLettersPerWord = Defaults.MIN_LETTERS_PER_WORD;
	private int maxLettersPerWord = Defaults.MAX_LETTERS_PER_WORD;

	public int getMinLettersPerWord() {
		return minLettersPerWord;
	}

	public int getMaxLettersPerWord() {
		return maxLettersPerWord;
	}

	public int getMaxWordsPerAnagram() {
		return maxWordsPerAnagram;
	}

	public void setMaxLettersPerWord(int maxLettersPerWord) {
		this.maxLettersPerWord = maxLettersPerWord;
	}

	public void setMaxWordsPerAnagram(int maxWords) {
		this.maxWordsPerAnagram = maxWords;
	}

	public void setMinLettersPerWord(int minLetters) {
		this.minLettersPerWord = minLetters;
	}

	public void setExcludedWords(Set<String> wordSet) {
		excludedWords.clear();
		if (wordSet != null) {
			for (String word : wordSet) {
				if (!Strings.isNullOrEmpty(word)) {
					excludedWords.add(WordUtils.canonicalizeString(word));
				}
			}
		}
	}

	public void setIncludedWords(Set<String> wordSet) {
		includedWords.clear();
		if (wordSet != null) {
			for (String word : wordSet) {
				if (!Strings.isNullOrEmpty(word)) {
					includedWords.add(WordUtils.canonicalizeString(word));
				}
			}
		}
	}

	public int getMaxAnagrams() {
		return maxAnagrams;
	}

	public void setMaxAnagrams(int maxAnagrams) {
		this.maxAnagrams = maxAnagrams;
	}

	public static Anagrammer getFromFile(String filename) {
		Set<String> dict = WordUtils.getWordsFromFile(filename);
		if (dict == null) {
			throw new IllegalArgumentException("Could not read dictionary at file path: " + filename);
		}
		return new Anagrammer(dict);
	}

	private Anagrammer(Set<String> dictionary) {
		this.dictionary = dictionary;
		this.histogram = WordUtils.buildHistogram(dictionary);

		for (String s : dictionary) {
			root.insert(flatten(s), s);
		}
	}

	public List<String> getCandidates(String input) {
		final String sourceString = WordUtils.canonicalizeString(input);
		return dictionary.parallelStream()
				.filter(candidate -> WordUtils.isAnagramSubstring(candidate, sourceString))
				.collect(Collectors.toList());
	}

	public List<String> getAnagrams(String input) {
		List<String> output = new ArrayList<String>();

		LinkedList<Character> workingChars = flatten(WordUtils.canonicalizeString(input));

		boolean supportsInclusionWords = eatIncluded(workingChars);
		if (!supportsInclusionWords) {
			return output;
		}

		buildAnagrams(workingChars, output, root, 0, includedWords.size());

		output = WordUtils.pruneDuplicateAnagrams(output);

		if (includedWords.size() > 0) {
			String prependText = Joiner.on(" ").join(includedWords) + " ";
			for (int i = 0; i < output.size(); ++i) {
				output.set(i, prependText + output.get(i));
			}
		}

		return output;
	}

	private boolean eatIncluded(LinkedList<Character> input) {
		String allWords = Joiner.on("").join(includedWords);
		LinkedList<Character> includes = flatten(allWords);

		Iterator<Character> includesIt = includes.iterator();
		Iterator<Character> inputIt = input.iterator();

		Comparator<Character> charComparator = getCharacterComparator();
		while (includesIt.hasNext()) {
			Character c = includesIt.next();
			while (inputIt.hasNext()) {
				Character i = inputIt.next();
				int compare = charComparator.compare(c, i);
				if (compare == 0) {
					inputIt.remove();
					break;
				}
				if (compare < 0) {
					return false;
				}
			}
		}

		return true;
	}

	private void buildAnagrams(List<Character> workingChars, List<String> output, TrieNode node, int characterDepth, int wordDepth) {

		if (wordDepth >= maxWordsPerAnagram) {
			return;
		}

		if (node == null || node.children == null) {
			return;
		}

		int numToExamine = node == root ? 1 : workingChars.size();
		for (int i = 0; i < numToExamine; ++i) {
			char c = workingChars.get(i);
			if (i != 0 && c == workingChars.get(i - 1)) {
				continue;
			}

			TrieNode n = node.getChild(c);
			if (n == null) {
				continue;
			}

			List<Character> constricted = new ArrayList<Character>(workingChars);
			constricted.remove(i);
			if (n.matches != null && characterDepth >= minLettersPerWord - 1 && characterDepth < maxLettersPerWord) {
				List<String> subOutput = new ArrayList<String>();

				if (constricted.size() != 0) {
					// Found a full word, start trying to read another word
					buildAnagrams(constricted, subOutput, root, 0, wordDepth + 1);
				}

				for (String w1 : n.matches) {
					if (excludedWords.contains(w1)) {
						continue;
					}

					for (String w2 : subOutput) {
						if (output.size() >= maxAnagrams) {
							return;
						}
						output.add(w1 + " " + w2);
					}

					if (constricted.size() == 0) {
						if (output.size() >= maxAnagrams) {
							return;
						}
						output.add(w1);
					}
				}
			}

			if (constricted.size() != 0) {
				buildAnagrams(constricted, output, n, characterDepth + 1, wordDepth);

				if (output.size() >= maxAnagrams) {
					return;
				}
			}
		}
	}

	/**
	 * Converts a string into a sorted list of characters.
	 *
	 * @param s
	 *            The input string.
	 * @return A list of characters that were in the string, sorted by their frequency in the dictionary.
	 */
	private LinkedList<Character> flatten(String s) {
		LinkedList<Character> temp = new LinkedList<Character>();
		for (int i = 0; i < s.length(); ++i) {
			temp.add(s.charAt(i));
		}
		Collections.sort(temp, getCharacterComparator());
		return temp;
	}

	private Comparator<Character> getCharacterComparator() {
		return (o1, o2) -> {
			int firstlevel = histogram.get(o1).compareTo(histogram.get(o2));
			if (firstlevel != 0) {
				return firstlevel;
			} else {
				return o1.compareTo(o2);
			}
		};
	}

	private static class TrieNode {
		List<String> matches;
		TrieNode[] children;

		TrieNode() {
			matches = null;
			children = null;
		}

		TrieNode getChild(Character c) {
			return children[c - 'a'];
		}

		void setChild(Character c, TrieNode n) {
			children[c - 'a'] = n;
		}

		void insert(Queue<Character> s, String actual) {
			if (s == null || 0 == s.size()) {
				return;
			}

			if (children == null) {
				children = new TrieNode[26];
			}

			char c = s.remove();
			TrieNode n = getChild(c);

			if (n == null) {
				n = new TrieNode();
				setChild(c, n);
			}

			if (0 != s.size()) {
				n.insert(s, actual);
			} else {
				if (n.matches == null) {
					n.matches = new ArrayList<String>();
				}
				n.matches.add(actual);
			}
		}
	}
}
