package com.what.anagram.core;

public class Defaults {
	public static final int MAX_ANAGRAMS = 5000;
	public static final int MAX_WORDS_PER_ANAGRAM = 10;
	public static final int MIN_LETTERS_PER_WORD = 1;
	public static final int MAX_LETTERS_PER_WORD = 31;

	public static final String DICTIONARY_PATH = "dict.txt";

	private Defaults() {
	}
}
