package com.what.anagram.core;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.common.base.Strings;
import com.google.common.io.Resources;

public class WordUtils {
	private WordUtils() {
	}

	public static Map<Character, Integer> buildHistogram(Set<String> words) {
		Map<Character, Integer> histogram = new TreeMap<Character, Integer>();
		for (String s : words) {
			for (int i = 0; i < s.length(); ++i) {
				char c = s.charAt(i);
				if (!histogram.containsKey(c)) {
					histogram.put(c, 1);
				} else {
					histogram.put(c, histogram.get(c) + 1);
				}
			}
		}
		return histogram;
	}

	public static Set<String> getWordsFromFile(String filename) {
		try {
			URL url = Resources.getResource(filename);
			String text = Resources.toString(url, Charsets.UTF_8);

			Set<String> dictionary = Arrays.stream(text.split("\n"))
					.map(WordUtils::canonicalizeString)
					.filter(line -> !Strings.isNullOrEmpty(line))
					.collect(Collectors.toSet());

			return dictionary;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static boolean isAnagramSubstring(String candidate, String source) {
		if (candidate == null || source == null) {
			return false;
		}

		if (candidate.length() > source.length()) {
			return false;
		}

		for (int i = 0; i < candidate.length(); i++) {
			String c = candidate.substring(i, i + 1);
			if (!source.contains(c)) {
				return false;
			} else {
				source = source.replaceFirst(c, "");
			}
		}

		return true;
	}

	/**
	 * Removes duplicate anagrams from the provided collection.
	 *
	 * @param anagramList
	 *            The list of anagrams.
	 * @return A new list without any duplicate anagrams.
	 */
	public static List<String> pruneDuplicateAnagrams(Collection<String> anagramList) {
		return anagramList.parallelStream().map(WordUtils::sortWords).distinct().collect(Collectors.toList());
	}

	public static String sortWords(String src) {
		String[] tokens = src.split("\\s+");
		Arrays.sort(tokens);
		return Joiner.on(" ").join(tokens);
	}

	/**
	 * Returns a string that only contains the alphabetic (A-Z, a-z) characters from the source string.
	 * 
	 * @param value
	 *            The string to canonicalize.
	 * @return The sanitized result.
	 */
	public static String canonicalizeString(String value) {
		value = value.toLowerCase(Locale.US);
		value = value.replaceAll("[^A-Za-z]", "");
		return value;
	}

	/**
	 * Converts a string to Title Case.
	 *
	 * @param src
	 *            The string to convert.
	 * @return The same string, in Title Case.
	 */
	public static String toTitleCase(String src) {
		// These cause the character following to be capitalized
		final String DELIMITERS = " '-/";

		StringBuilder sb = new StringBuilder();
		boolean capNext = true;

		for (char c : src.toCharArray()) {
			c = (capNext) ? Character.toUpperCase(c) : Character.toLowerCase(c);
			sb.append(c);
			capNext = (DELIMITERS.indexOf(c) >= 0);
		}
		return sb.toString();
	}
}
