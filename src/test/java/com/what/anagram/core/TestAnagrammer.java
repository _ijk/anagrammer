package com.what.anagram.core;

import java.util.List;

import org.junit.Test;

public class TestAnagrammer {

	@Test
	public void testLoadDictionary() {
		Anagrammer trie = Anagrammer.getFromFile(Defaults.DICTIONARY_PATH);
		List<String> anagrams = trie.getAnagrams("sample text");
		anagrams.stream().map(WordUtils::toTitleCase).forEachOrdered(System.out::println);
	}
}
